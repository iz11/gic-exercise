FROM   registry.access.redhat.com/ubi8/ubi:8.0

MAINTAINER   SIN <sindy.susanti@izeno.com>

# command line options to pass to the JVM
ENV	  JAVA_OPTIONS -Xmx512m
ENV APP_DIR=/opt/app-root   

WORKDIR ${APP_DIR}

# Install the Java runtime, create a user for running the app, and set permissions
RUN   yum install -y --disableplugin=subscription-manager java-1.8.0-openjdk-headless && \
      yum clean all --disableplugin=subscription-manager -y    
	  
# Copy the runnable fat JAR to the container.
COPY target/*.jar ${APP_DIR}/app.jar

COPY override/*.properties ${APP_DIR}/override.properties

RUN   chgrp -R 0 ${APP_DIR} && \
      chmod -R g+rwx ${APP_DIR}

EXPOSE 8080

USER  1001

# Run the fat JAR
ENTRYPOINT ["java","-jar","app.jar"]
