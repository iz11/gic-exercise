mvn clean install

docker login -u izenoocpadmin -p$(oc whoami -t) default-route-openshift-image-registry.apps.ocpprd.izeno.biz

docker build --rm -t default-route-openshift-image-registry.apps.ocpprd.izeno.biz/sinchan-project/my-spring-app:latest .

docker push default-route-openshift-image-registry.apps.ocpprd.izeno.biz/sinchan-project/my-spring-app:latest